lb_finder.py
------------

`lb_finder.py` is a small program for identifying tips in a Newick tree that
exceeds a user-provided treshold.

The functionality was inspired by the `Long Branch Finder` utility within the
[Osiris toolkit for phylogenetics](https://doi.org/10.1186/1471-2105-15-230).
Osiris is employed on the [Galaxy platform](https://usegalaxy.org/) and thus
`lb_finder.py` is meant to be a stand-alone alternative to `Long Branch
Finder`.

### Usage

```
usage:
  Parse the Newick tree in <path>, identify tips that are longer than
  <factor> times the standard deviations of all tips and report their name and
  branch length, separated by tab, to standard output (stdout).

positional arguments:
  path        The file containing the tree in Newick format
  factor      The number of standard deviations to use as a treshold.

optional arguments:
  -h, --help  show this help message and exit
```
